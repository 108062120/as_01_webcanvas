var is_draw; // true if drawing
var cv, pen, pen_width = 1;
var colorWell, cool_color = "#f47be6";
var mode = "pen";
var init_X, init_Y;
var img_history = [];
var redo_img_history = [];
var current_img;
var undoing = false;
var drawing_cool_shape = false;
var imageLoader;
var slider;
var is_filled = false;
var textToWrite = "";
var textarea, textfont, textsize;

window.addEventListener("load", init, false);

function updateFirst(event) {
    pen.strokeStyle = event.target.value;
    pen.fillStyle = event.target.value;
    cool_color = event.target.value;
}

function updateAll(event) {
    document.querySelectorAll("cv").forEach(function(p) {
        pen.strokeStyle = event.target.value;
        pen.fillStyle = event.target.value;
    });
}

function init() {
    cv = document.getElementById("cv");
    pen = cv.getContext("2d");
    cv.width = 600;
    cv.height = 450;
    current_img = pen.getImageData(0, 0, cv.width, cv.height);
    cv.style["cursor"] = "crosshair";
    pen.font = "20pt Arial";
    textarea = document.getElementById("textarea");
    textfont = document.getElementById("textfont");
    textsize = document.getElementById("textsize");

    colorWell = document.querySelector("#favcolor");
    colorWell.value = cool_color;
    pen.strokeStyle = cool_color;
    pen.fillStyle = cool_color;
    colorWell.addEventListener("input", updateFirst, false);
    colorWell.addEventListener("change", updateAll, false);
    colorWell.select();

    imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);

    slider = document.getElementById("myRange");
    pen.lineWidth = slider.value;
}

function linewidth() {
    pen.lineWidth = document.getElementById("myRange").value;

}

window.addEventListener("keydown", function(event) {
    if (event.defaultPrevented) {
        return; // Do nothing if the event was already processed
    }

    switch (event.key) {
        case "Enter":
            pen.strokeStyle = cool_color;
            pen.fillStyle = cool_color;
            textToWrite = textarea.value;
            pen.font = textsize.value + "pt " + textfont.value;
            pen.fillText(textToWrite, init_X, init_Y);
            textarea.value = "";
            textToWrite = "";
            textarea.style["opacity"] = 0;
            break;
        default:
            return; // Quit when this doesn't handle the key event.
    }

    // Cancel the default action to avoid it being handled twice
    event.preventDefault();
}, true);

function mouseDown() {
    if (textarea.style["opacity"] == 6) {
        textarea.style["opacity"] = 0;
    }
    pen.moveTo(event.offsetX, event.offsetY);
    init_X = event.offsetX;
    init_Y = event.offsetY;
    is_draw = true;
    pen.beginPath();
    if (mode == "text") {
        textarea.style.left = init_X + "px";
        textarea.style.top = init_Y + 20 + "px";
        textarea.style["opacity"] = 6;
        is_draw = false;
        pen.closePath();
    }

    img_history.push(current_img);
    if (drawing_cool_shape)
        drawing_cool_shape = false;
    current_img = pen.getImageData(0, 0, cv.width, cv.height);

    if (undoing) {
        redo_img_history = [];
        undoing = false;
    }
}

function mouseMove() {
    if (is_draw) {
        textarea.style["opacity"] = 0;
        pen.lineCap = document.getElementById("brush").value;
        if (mode == "pen") {
            pen.globalCompositeOperation = "source-over";
            pen.lineTo(event.offsetX, event.offsetY);
            pen.stroke();
        } else
        if (mode == "line") {
            pen.globalCompositeOperation = "source-over";
            if (drawing_cool_shape) {
                pen.putImageData(current_img, 0, 0);
            }
            pen.beginPath();
            pen.moveTo(init_X, init_Y);
            pen.lineTo(event.offsetX, event.offsetY);
            pen.stroke();
            drawing_cool_shape = true;
        } else if (mode == "rectangle") {
            pen.globalCompositeOperation = "source-over";
            if (drawing_cool_shape) {
                pen.putImageData(current_img, 0, 0);
            }
            pen.beginPath();
            if (!is_filled) {
                pen.strokeRect(init_X, init_Y, (event.offsetX - init_X), (event.offsetY - init_Y));
            } else {
                pen.fillRect(init_X, init_Y, (event.offsetX - init_X), (event.offsetY - init_Y));
            }
            pen.closePath();
            drawing_cool_shape = true;
        } else if (mode == "circle") {
            pen.globalCompositeOperation = "source-over";
            if (drawing_cool_shape) {
                pen.putImageData(current_img, 0, 0);
            }
            pen.beginPath();
            if (!is_filled) {
                pen.arc(((event.offsetX - init_X) / 2) + init_X, ((event.offsetY - init_Y) / 2) + init_Y, Math.abs(event.offsetX - init_X), 0, 2 * Math.PI);
                pen.stroke();
                pen.closePath();
            } else {
                pen.arc(((event.offsetX - init_X) / 2) + init_X, ((event.offsetY - init_Y) / 2) + init_Y, Math.abs(event.offsetX - init_X), 0, 2 * Math.PI);
                pen.fill();
            }
            drawing_cool_shape = true;
        } else if (mode == "text") {
            pen.globalCompositeOperation = "source-over";
        } else if (mode == "triangle") {
            pen.globalCompositeOperation = "source-over";
            if (drawing_cool_shape) {
                pen.putImageData(current_img, 0, 0);
            }
            pen.beginPath();
            if (!is_filled) {
                pen.moveTo(init_X, init_Y);
                pen.lineTo(event.offsetX, event.offsetY);
                pen.lineTo(init_X - (event.offsetX - init_X), event.offsetY);
                pen.lineTo(init_X, init_Y);
                pen.stroke();
                pen.closePath();
            } else {
                pen.moveTo(init_X, init_Y);
                pen.lineTo(event.offsetX, event.offsetY);
                pen.lineTo(init_X - (event.offsetX - init_X), event.offsetY);
                pen.lineTo(init_X, init_Y);
                pen.fill();
            }
            drawing_cool_shape = true;
        } else if (mode == "eraser") {
            pen.globalCompositeOperation = "destination-out";
            pen.lineTo(event.offsetX, event.offsetY);
            pen.stroke();
            pen.globalCompositeOperation = "source-over";
        }
    }
}

function mouseUp() {
    is_draw = false;
    pen.closePath();
    if (drawing_cool_shape)
        drawing_cool_shape = false;
    current_img = pen.getImageData(0, 0, cv.width, cv.height);
}

function refresh() {
    location.reload();
}

function Pen() {
    cv.style["cursor"] = "crosshair";
    mode = "pen";
}

function line() {
    cv.style["cursor"] = "crosshair";
    mode = "line";
}

function Eraser() {
    cv.style["cursor"] = "url(001.ico), auto";
    mode = "eraser";
}

function Rectangle() {
    cv.style["cursor"] = "crosshair";
    mode = "rectangle";
}

function Undo() {
    if (img_history.length > 0) {
        redo_img_history.push(current_img);
        current_img = img_history.pop();
        pen.putImageData(current_img, 0, 0);
        undoing = true;
    }
}

function Redo() { //加油ˊˇˋ 
    if (redo_img_history.length > 0) {
        img_history.push(current_img);
        current_img = redo_img_history.pop();
        pen.putImageData(current_img, 0, 0);
    }
}

function Circle() {
    cv.style["cursor"] = "crosshair";
    mode = "circle";
}

function Triangle() {
    cv.style["cursor"] = "crosshair";
    mode = "triangle";
}


function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function(event) {
        var img = new Image();
        img.onload = function() {
            cv.width = img.width;
            cv.height = img.height;
            pen.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    init();
}

function Fill() {
    is_filled = !is_filled;
    if (!is_filled) {
        document.getElementById("fillbtn").innerHTML = "Fill";
    } else {
        document.getElementById("fillbtn").innerHTML = "Stroke";
    }
}

function text() {
    cv.style["cursor"] = "text";
    mode = "text";
    mouseMove();
    pen.fillText(textToWrite, event.offsetX, event.offsetY);
}