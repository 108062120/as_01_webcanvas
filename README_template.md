# Software Studio 2021 Spring
## Assignment 01 Web Canvas 108062120


### Scoring

| **Basic components** | **Score** | **Check** |
|:-------------------- |:---------:|:---------:|
| Basic control tools  |    30%    |     Y     |
| Text input           |    10%    |     Y     |
| Cursor icon          |    10%    |     Y     |
| Refresh button       |    10%    |     Y     |

| **Advanced tools**     | **Score** | **Check** |
|:---------------------- |:---------:|:---------:|
| Different brush shapes |    15%    |     Y     |
| Un/Re-do button        |    10%    |     Y     |
| Image tool             |    5%     |     Y     |
| Download               |    5%     |     Y     |

| **Other useful widgets**   | **Score** | **Check** |
|:-------------------------- |:---------:|:---------:|
| Brush Shape: Straight Line |   1~5%    |     Y     |
| Line Cap                   |           |     Y     |
| Fill Up Shapes             |           |     Y     |


---

### How to use 

1. Click **"Pen"** to draw as you like. Make yourself at home.
2. If found somthing wrong, you can click **"Eraser"** to erase things makes you crazy on the canvas. (It can't erase shitty things in your life, I'm sorry.)
3. Or you can click **"Undo"** to make those stains disappear and the canvas will return to the canvas before you fucked up.
4. If you click too many times of **"Undo"** (fucked up again, poor you help QQ), don't be nervous, you can still click **"Redo"** to have your adorable paintings back.
5. If the canvas becomes as horrible as your life, click **"Refresh"** and you will get a brand new canvas. (It can't refresh your life. I'm sorry, again.)
6. The default type of line-cap is *"butt"*, if you don't like it, click the dropdown menu at the left of **"Pen"**. There's no many options, so you don't have to deal with your choosing barriers.
7. If you need some help to draw a perfect **"Line"**, **"Rectangle"**, **"Circle"**, or **"Triangle"**, click those buttons. Please draw other shapes by your own hands, do not always rely on others. You are a grown people, be independent, mommy won't always stay with you.
8. Click **"Fill/Stroke"** if you want the shapes filled or with border only. However,  it can't fill your life with happiness or any color, and still, your liver stays black.
9. If you need some text exist on your canvas, click **"Text"** and move the cursor to the position you want the words to stay. After typing, press *'Enter'* on your keyboard. You should find the words now stick on the canvas. Magic!
10. The default font of text is *"Arial"* and the size is *"20"*, if those settings are boring to you, press the dropdown menu at the right of **"Text"** and make the font not so common. That's all you can do. You are still a commoon people with so much bullshit to absorb every single day.
11. After finishing your masterpiece, you can download and save it by clicking **"Save"**.
12. If you already have a draft, you are able to upload it by clicking **"選擇檔案"**. The width and height of canvas will adapt for your draft. Yet, you will never be capable to adapt to the brutal society.
13. The default width of lines is *'1'*, you can make it rougher by grabbing the slider handle under the buttons. Be careful, your life may become rougher, too.
14. In order to change the color, please click the cute rectangle at the buttom. The canvas is the only colorful thing in your life, please cherish it.

### Function description

1. Click **“Fill/Stroke”** if you want the shapes filled or with border only.
2. The default type of line-cap is *“butt”*, if you don’t like it, click the dropdown menu at the left of **“Pen”**. 
3. If you need some help to draw a straight **“Line”**, , click the button.

### Gitlab page link

https://108062120.gitlab.io/as_01_webcanvas

### Others (Optional)
CSS is so hard just like my life. 
![](https://i.imgur.com/YqjOaKo.jpg)

I love Google and StackOverflow.
![](https://i.imgur.com/ZscbSKc.jpg)

助教們辛苦ㄌ！
![](https://i.imgur.com/uz6QQ1u.jpg)


<style>
table th{
    width: 100%;
}
</style>